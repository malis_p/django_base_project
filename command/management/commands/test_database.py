from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from profil.models import Profil, ProfilGroup


def test_profil_group():
        try:
                dev = ProfilGroup.objects.get(name="dev")
                print("dev succeed")
        except ProfilGroup.DoesNotExist:
                print("dev failed")
        try:
                user = ProfilGroup.objects.get(name="user")
                print("user succeed")
        except ProfilGroup.DoesNotExist:
                print("user failed")
        try:
                admin = ProfilGroup.objects.get(name="admin")
                print("admin succeed")
        except ProfilGroup.DoesNotExist:
                print("admin failed")

class Command(BaseCommand):
    help= 'test database'

    #def add_arguments(self, parser):
        #parser.add_argument('--folder', dest='folder', type=str, default=False, help='REQUIRED: sepcify the path to the folder')
        #parser.add_argument('--update', dest='update',  action='store_true', default=False, help='OPTIONNAL: should the entity exist, it will be updated')
        
    def handle(self, *args, **options):
     #   if options['folder'] and options['update']:
            test_profil_group()
