from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

import csv

from profil.models import Profil, ProfilGroup

def export_profil_group(path):
        print("exporting profil group")
        group_path =  path + "/profil_group.csv"
        group_list = ProfilGroup.objects.all()
        with open(group_path, 'w+') as file:
                writer = csv.writer(file, delimiter=';', quotechar='|')
                writer.writerow(['name']);
                for current_group in group_list:
                        writer.writerow([current_group.name])
                        print("Group  [" + current_group.name + "]")
                        
def export_profil(path):
        print("exporting profil")
        profil_path = path + "/profil.csv"
        profil_list = Profil.objects.all()
        with open(profil_path, 'w+') as file:
                writer = csv.writer(file, delimiter=';', quotechar='|')
                writer.writerow(['pseudo', 'email', 'password', 'first_name', 'last_name', 'is_active', 'is_confirmed', 'confirmation_key', 'key_date','last_login', 'group']);
                for current_profil in profil_list:
                        writer.writerow([current_profil.pseudo, current_profil.email, current_profil.password, current_profil.first_name, current_profil.last_name,
                                         current_profil.is_active, current_profil.is_confirmed,
                                         current_profil.confirmation_key, current_profil.key_date, current_profil.last_login, current_profil.group.name])
                        print("Profil [" + current_profil.pseudo + "]")
                         
def export_base(path):
        print ("export database on [" + path + "], update = [True]")
        export_profil(path)
        export_profil_group(path)
        print("--------------------------------")
    #import_competence(path, update)
    #print("--------------------------------")
    #import_patient(path, update)
    #print("--------------------------------")
    
class Command(BaseCommand):
    help= 'import database, arguments are : {--folder "path"}'

    def add_arguments(self, parser):
        parser.add_argument('--folder', dest='folder', type=str, default=False, help='REQUIRED: sepcify the path to the folder')
        
    def handle(self, *args, **options):
        if options['folder']: 
            export_base(options['folder'])
        else:
            print("error ! you must specify a folder (string), default = --folder data/csv/tmp1")
