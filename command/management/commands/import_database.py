from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from command.create_entity import create_profil, create_profil_group

import csv

#name
def import_profil_group(path, update):
        print("importing profilGroup")
        group_path = path + "/profil_group.csv"
        with open(group_path) as file:
                reader = csv.reader(file, delimiter=';', quotechar='|')
                reader.next()
                for row in reader:
                        create_profil_group(update, row[0])

# username_str, email_str, password_str, first_name_str, last_name_str, is_active_str, is_confirmed_str, confirmation_key_str, key_date, last_login_str, group_str
def import_profil(path, update):
        print("importing profil")
        profil_path = path + "/profil.csv"
        with open(profil_path) as file:
            reader = csv.reader(file, delimiter=';', quotechar='|')
            reader.next()
            for row in reader:
                create_profil(update, row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10])

                                                
def import_base(path, update):
    if update == True:
        print ("import database on [" + path + "], update = [True]")
    else:
        print ("import database on [" + path + "], update = [False]")


    import_profil_group(path, update)
    import_profil(path, update)
    print("--------------------------------")
    #import_competence(path, update)
    #print("--------------------------------")
    #import_patient(path, update)
    #print("--------------------------------")
        
    
class Command(BaseCommand):
    help= 'import database, arguments are : {--folder "path"} {--update}'

    def add_arguments(self, parser):
        parser.add_argument('--folder', dest='folder', type=str, default=False, help='REQUIRED: sepcify the path to the folder')
        parser.add_argument('--update', dest='update',  action='store_true', default=False, help='OPTIONNAL: should the entity exist, it will be updated')
        
    def handle(self, *args, **options):
        if options['folder'] and options['update']:
            import_base(options['folder'], options['update'])
        elif options['folder']:
            import_base(options['folder'], False)
        else:
            print("error ! you must specify a folder (string), default = --folder data/tmp1")
