from django.core.management.base import BaseCommand, CommandError
from profil.models import Profil
from django.contrib.auth.models import User

from datetime import datetime

def delete_profils():
    profils = Profil.objects.all()
    profils.delete()

def delete_users():
    users = User.objects.all()
    print("--")
    users.delete()
