from django.core.management.base import BaseCommand, CommandError
from profil.models import Profil, ProfilGroup
from datetime import datetime

def get_date_from_str(date_str):
    print ("date str = [" + date_str + "]")
    return datetime.now()#datetime.strptime(date_str, '%Y-%b-%d %H:%M:%S').date()

def fill_profil(profil, username_str, email_str, password_str, first_name_str, last_name_str, is_active_str, is_confirmed_str, confirmation_key_str, key_date_str, last_login_str, group_str):
    profil.pseudo = username_str
    profil.email = email_str
    profil.password = password_str
    profil.first_name = first_name_str
    profil.last_name = last_name_str
    profil.is_active = is_active_str
    profil.is_confirmed = is_confirmed_str
    profil.confirmation_key = confirmation_key_str
    profil.key_date = get_date_from_str(key_date_str)
    profil.last_login = get_date_from_str(last_login_str)
    group = ProfilGroup.objects.get(name=group_str)
    if group == None:
        return None
    else:
        profil.group = group
        return profil

def create_profil_group(update, name_str):
    try:
        group = ProfilGroup.objects.get(name=name_str)
        if update == True:
            #group = fill_profil_group(group, name_str)
            group.save()
            print ("Update  : Group   [" + name_str +"]")
        else:
            print ("Warning : Group   [" + name_str + "]")
    except ProfilGroup.DoesNotExist:
        group = ProfilGroup()
        group.name = name_str
        #group = fill_group
        group.save()
        print ("Succes    : Group   [" + name_str + "]")

def create_profil(update, username_str, email_str, password_str, first_name_str, last_name_str, is_active_str, is_confirmed_str, confirmation_key_str, key_date_str, last_login_str, group_str):
    try:
        profil = Profil.objects.get(pseudo=username_str)
        if update == True:
            profil = fill_profil(profil, username_str, email_str, password_str, first_name_str, last_name_str, is_active_str,
                                 is_confirmed_str, confirmation_key_str, key_date_str, last_login_str, group_str)
            if profil == None:
                print("Error     : Profil  [" + username_str + "]")
            else:
                profil.save()
                print ("Update  : Profil  [" + username_str + "]")
        else:
            print ("Warning : Profil  [" + username_str + "]")
    except Profil.DoesNotExist:
        profil =  Profil()#.objects.create_user(username_str, email_str, password_str)
        profil =  fill_profil(profil, username_str, email_str, password_str, first_name_str, last_name_str, is_active_str,
                              is_confirmed_str, confirmation_key_str, key_date_str, last_login_str, group_str)
        if profil == None:
            print("Error     : Profil  [" + username_str + "]")
        else:
            profil.save()
            print ("Succes   : Profil  [" + username_str + "] created !")
