from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from common import views_main

if settings.WEBSITE_STATUS == True: 
    urlpatterns = [
        url(r'^' + settings.WEBSITE_NAME_SLUG + '/', include ('common.urls')),
        url(r'^profil/', include ('profil.urls')),
        url(r'^forum/', include ('forum.urls')),
        #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')) 
    ]
else:
    urlpatterns = [
        url(r'^.*$', views_main.maintenance),
    ]
