from django import forms
from profil.models import Profil

class UserConnectionForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

class UserSubscriptionForm(forms.Form):
    email = forms.EmailField()
    username = forms.CharField(max_length=150, label='Nom d\'utilisateur')
    password = forms.CharField(widget=forms.PasswordInput(),label='Mot de passe')
    password_confirm = forms.CharField(widget=forms.PasswordInput(), label='Retapez votre mot de passe')
  
class ChangeEmailForm(forms.Form):
    new_email = forms.EmailField()
    
class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput())
    new_password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

class ChangePhotoForm(forms.Form):
    photo = forms.ImageField(label='Image de profil')
