from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils import timezone
from rest_framework import serializers

class ProfilManager(BaseUserManager):
    """
    fields
    """

    """
    methods
    """

    def create_user(self, username, email, password):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        group = ProfilGroup.objects.get(name="user")
        if group is None:
            raise ValueError('User Group Not Find')
            return None
        if not username:
            raise ValueError('The given username must be set')
        if not email:
            raise ValueError('The given email must be set')
        if not password:
            raise ValueError('The given password must be set')
        user = self.model(pseudo=username, email=email, last_login=now)
        user.set_password(password)
        user.group = group
        user.save(using=self._db)
        return user

    def create_super_user(self, username, email, password):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        if not email:
            raise ValueError('The given email must be set')
        if not password:
            raise ValueError('The given password must be set')
        user = self.model(pseudo=username, email=email, last_login=now)

        user.set_password(password)
        user.save(using=self._db)
        return user

class ProfilGroup(models.Model):

    """
    fields
    """
    name = models.CharField(max_length=16, db_column='name')

    """
    methods
    """
    
    def __unicode__( self ):
        return self.name
    
    class Meta:
        db_table = "UserGroup"

class Profil(AbstractBaseUser):

    """
    fields :
    """
    #id is auto generated
    #password is auto generated
    #last_login is auto generated
    email = models.EmailField('email address', unique=True, db_index=True, db_column='email')
    pseudo = models.CharField(unique=True, max_length=16, db_column='pseudo')
    first_name = models.CharField(max_length=16, db_column='first_name')
    last_name = models.CharField(max_length=16, db_column='last_name')
    is_active = models.BooleanField(default=True, db_column='is_active')
    is_confirmed = models.BooleanField(default=False, db_column='is_confirmed')
    confirmation_key = models.CharField(max_length=40, blank=True, db_column='confirmation_key')
    key_date = models.DateTimeField(default=datetime.now(), db_column='key_date')
    group = models.ForeignKey(ProfilGroup, db_column='group')

    
    USERNAME_FIELD = 'pseudo'

    objects = ProfilManager()

    """
    methods :
    """
    
    def __unicode__( self ):
        return self.pseudo

    @property
    def is_staff(self):
         "Is the user a member of staff?"
         return True
     
    class Meta:
        db_table = "User"
        

    #def create(self, pseudo, email, password)
    #print("profil create !!!")
    #profil = new Profil
    #return profil
