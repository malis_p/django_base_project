# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-13 13:29
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('profil', '0005_auto_20160413_0831'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profil',
            name='group',
        ),
        migrations.AddField(
            model_name='profil',
            name='group',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='profil.ProfilGroup'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='profil',
            name='key_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 4, 13, 13, 29, 47, 375158)),
        ),
    ]
