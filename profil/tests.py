from django.test import TestCase
from profil.models import Profil, ProfilGroup
# Create your tests here.

class ProfilGroupTestCase(TestCase):
    def setUp(self):
        print("init ProfilGroupTest")
        
    def test_group_are_init(self):
        """test group are init"""
        try:
            dev = ProfilGroup.objects.get(name="dev")
            print("dev succeed")
        except ProfilGroup.DoesNotExist:
            print("dev failed")
        try:
            user = ProfilGroup.objects.get(name="user")
            print("user succeed")
        except ProfilGroup.DoesNotExist:
            print("user failed")
        try:
            admin = ProfilGroup.objects.get(name="admin")
            print("admin succeed")
        except ProfilGroup.DoesNotExist:
            print("admin failed")
        print("test ended")
        #self.assertEqual(lion.speak(), 'The lion says "roar"')
        #self.assertEqual(cat.speak(), 'The cat says "meow"')
