from django.db import IntegrityError
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.http import HttpResponse

from django.utils import timezone
import datetime

import datetime, hashlib, random

from common.views_main import home

from profil.forms import UserConnectionForm, UserSubscriptionForm
from profil.models import Profil

from emailing.views_profil import send_confirmation, send_new_password

from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse

@api_view(['GET'])
#@authentication_classes((SessionAuthentication, BasicAuthentication))
#@permission_classes((IsAuthenticated,))
def example_view(request, format=None):
        content = {
            'user': unicode(request.user),  # `django.contrib.auth.User` instance.
            'auth': unicode(request.auth),  # None
        }
        return Response(content)

def profil_login(request):
    print("profil_login");
    
    if check_user_connected(request) == False:
        print("false");
        if request.method == 'POST':
            form = UserConnectionForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                user = authenticate(username=username, password=password)
                profil = get_profil_from_user(user)
            
                if user is not None and profil is not None and profil.is_confirmed == True:
                    login(request, user)
                    print("login");
                elif user is not None and profil is not None and profil.is_confirmed == False:
                    login(request, user)
                    print("WARNING NOT CONFIRMED !!!")
                    #form.add_error('username', 'votre compte n est pas encore actif, veuillez regarder votre boite mail')
                    #return render(request, 'profil/login.html', {'form': form, } )
                else:
                    print("user is none");
                    form.add_error('username', 'combinaison pseudo / mot de passe incorrecte !')
                    return render(request, 'profil/login.html', {'form': form, })
                return redirect (home)
        else:
            form = UserConnectionForm()
            return render(request, 'profil/login.html', {'form': form, })
    else:
        print("true");
        return redirect(home);
    
def profil_logout(request):
    logout(request)
    return redirect(home)

def profil_show(request):
    profil = get_current_profil(request)
    if profil != None:
        return render(request, 'profil/show.html', {'profil':profil })
    return redirect(home)
