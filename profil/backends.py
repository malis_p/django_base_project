from .models import Profil

class ProfilAuthBackend(object):

    # auth with password & pseudo
    def authenticate(self, username=None, password=None):
        try:
            user = Profil.objects.get(pseudo=username)
            if user.check_password(password):
                return user
        except Profil.DoesNotExist:
            return None
        return None

    # return user with id
    def get_user(self, user_id):
        try:
            return Profil.objects.get(id=user_id)
        except Profil.DoesNotExist:
            return None
        return None
