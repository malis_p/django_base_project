from django.db import IntegrityError
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.http import HttpResponse
from forum.forms import AddArticleForm, AddPostForm
from django.utils import timezone
import datetime


import datetime, hashlib, random


from common.views_main import home
from forum.views_forum import home as forum_home

from forum.forms import ChangeProfilGroupForm
from forum.models import Article, Post
from profil.models import Profil
from profil.views_decorator import req_profil
from emailing.views_profil import send_confirmation, send_new_password

@req_profil({"login": True, "confirmed": True})
def article_list(request, profil):
    articles = Article.objects.all()
    return render(request, 'forum/article_list.html', {'list_article': articles})

@req_profil({"login": True, "confirmed": True})
def article_show(request, profil, article_id):
    current_article = Article.objects.get(id=article_id)
    if current_article is None:
        return redirect(forum_home)
    post_list = Post.objects.filter(article=current_article)
    return render(request, 'forum/article_show.html', {'article': current_article, 'post_list': post_list})

@req_profil({"login": True, "confirmed": True})
def add_article(request, profil):
    if request.method == 'POST':
        form = AddArticleForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            user = request.user
            article = Article()
            article.user = user
            article.name = name
            article.save()
            return redirect(add_post)
    else:
        form = AddArticleForm()
    return render(request, 'forum/add_article.html', {'form': form})

@req_profil({"login": True, "confirmed": True})
def add_post(request, profil, article_id):

    article = Article.objects.get(id=article_id)
    if article is None:
        return redirect (article_list)
    post_list = Post.objects.filter(article=article)
    if request.method == 'POST':
        form = AddPostForm(request.POST)
        if form.is_valid():

            user = request.user
            data = form.cleaned_data['data']
            pos = article.get_next_pos()
            
            post = Post()
            post.data = data
            post.article = article
            post.user = user
            post.pos = pos
            post.save()
            return redirect (article_show, article_id=article_id)
    else:
        form = AddPostForm()
    return render(request, 'forum/add_post.html', {'form': form, 'post_list': post_list})

@req_profil({"login": True, "confirmed": True})
def home(request, profil):
    return render(request, 'forum/home.html', {})

@req_profil({"login": True, "confirmed": True})
def profil_list(request, profil):
    profil_array = Profil.objects.all()
    return render(request, 'forum/profil_list.html', { 'profil_array': profil_array,})

@req_profil({"login": True, "confirmed": True})
def profil_show(request, profil, profil_id):
    current_profil = Profil.objects.get(id=profil_id)
    if current_profil is None:
        return redirect(forum_home)
    articles = Article.objects.filter(user=current_profil)
    for article in articles:
        print("article = [" + article.name + "]")
    return render(request, 'forum/profil_show.html', {'current_profil': current_profil, 'articles': articles})

@req_profil({"login": True, "confirmed": True})
def change_profil_group(request, profil, profil_id):
    target_profil = Profil.objects.get(id=profil_id)
    if request.method == 'POST':
        form = ChangeProfilGroupForm(request.POST)
        if form.is_valid():
            group_form = form.cleaned_data['group']
            target_profil.group = group_form
            return redirect (forum_home)
        else:
            return render(request, 'forum/change_profil_group.html', {'form': form, 'target_profil_id': profil_id })
    else:
        form=ChangeProfilGroupForm()
        return render(request, 'forum/change_profil_group.html', {'form': form, 'target_prodil_id': profil_id })
