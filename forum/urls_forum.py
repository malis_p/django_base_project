from django.conf.urls import patterns, include, url
from . import views_forum

urlpatterns = [
    url(r'^home$', views_forum.home, name='forum_home'),
    url(r'^article_list$', views_forum.article_list, name='forum_article_list'),
    url(r'^article_show/(?P<article_id>\d+)$', views_forum.article_show, name='forum_article_show'),
    url(r'^add_article$', views_forum.add_article, name='forum_add_article'),
    url(r'^add_post/(?P<article_id>\d+)$', views_forum.add_post, name='forum_add_post'),
    
    url(r'^profil_list$', views_forum.profil_list, name='forum_profil_list'),
    url(r'^profil_show/(?P<profil_id>\d+)$', views_forum.profil_show, name='forum_profil_show'),
    url(r'^change_profil_group/(?P<profil_id>\d+)$', views_forum.change_profil_group, name='forum_change_profil_group'),
#    url(r'^profil_show$', views_forum.profil_show, name='forum_profil_show'),
]
