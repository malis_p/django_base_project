from __future__ import unicode_literals
from profil.models import Profil
from django.db import models

class Article(models.Model):
    name = models.CharField(max_length=32, db_column='name')
    user = models.ForeignKey(Profil, on_delete=models.CASCADE, db_column='user')

    def get_next_pos(self):
        list_post = Post.objects.filter(article=self)
        if list_post == None:
            max_pos = 0
        else:
            max_pos = 0
            for post in list_post:
                if post.pos > max_pos:
                    max_pos = post.pos
        return max_pos + 1
        
    class Meta:
        db_table="forum_article"
    
class Post(models.Model):

    data = models.TextField(db_column='data')
    article = models.ForeignKey(Article, on_delete=models.CASCADE, db_column='article')
    user = models.ForeignKey(Profil, on_delete=models.CASCADE, db_column='user')
    pos = models.IntegerField(db_column='pos')

    class Meta:
        db_table="forum_post"
