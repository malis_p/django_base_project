from django import forms
from profil.models import ProfilGroup
from tinymce.widgets import TinyMCE

class ChangeProfilGroupForm(forms.Form):
   group = forms.ModelChoiceField(queryset=ProfilGroup.objects.all())

class AddArticleForm(forms.Form):
   name = forms.CharField(label='name', max_length=32)

class AddPostForm(forms.Form):
   data = forms.CharField(required=False, widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
